# Versioni varie

node: v4.4.3

# Note email composer

per android bisonga usare la versione 0.8.2  mentre per ios la master (0.8.3):

# Dopo il clone

per re-installatere tutte le dipendenze:

```
ionic state restore
``` 


## Per ios

```
#!bash
cordova plugin  add https://github.com/katzer/cordova-plugin-email-composer.git --save
```

## Per rimuovere il plugin

```
#!bash
 cordova plugin remove cordova-plugin-email-composer
```
 
 
## Per Android

```
#!bash
 cordova plugin add https://github.com/katzer/cordova-plugin-email-composer.git#0.8.2 --save
```

### Versione

Specificare qui  ```version="1.3.1"  ``` la versione visibile all'utente e qui: ```android-versionCode="320000" ``` la versione per  google play 

# Chiavi

## Android

Hyt098JKU178


## BUILD Android
Utilizzato il comando con cordova 6.0.0 per bug:

cordova platform add android@5.2.2