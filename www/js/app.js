// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('idromec', [ 'ionic',
                            'ngResource',
                            'ngCordova.plugins.launchNavigator',
                            'ngCordova',
                            'ngCordova.plugins.emailComposer',
                            'pascalprecht.translate',
                            'tmh.dynamicLocale',
                            'uiGmapgoogle-maps',
                            'ionicRipple',
                            'ksSwiper',
                            'idromec.config',
                            'idromec.splash',
                            'idromec.credits',
                            'idromec.menu',
                            'idromec.wordpress_pages',
                            'idromec.contact',
                            'idromec.notification'
                          ])

.constant('availableLanguages',['en-US','it-it'])
.constant('defaultLanguage','it-it')

.run(function($ionicPlatform, tmhDynamicLocale, $cordovaGlobalization, $translate, $http, $filter, $state, $q, $ionicPopup) {

  // // Set the language with the help of Angular Dynamic Locale
  function applyLanguage(language){
    tmhDynamicLocale.set(language.toLowerCase());
  }

  // Return the language among the available languages
  function getSuitableLanguage(language){
    for( var index = 0; index < availableLanguages.length;index ++){
      if(availableLanguages[index].toLowerCase() == language.toLowerCase()){
        return availableLanguages[index];
      }
    }
    return defaultLanguage;
  }

  function setLanguagePromise(){
    var deferred = $q.defer();
    if(typeof navigator.globalization !== 'undefined'){
      $cordovaGlobalization.getPreferredLanguage().then(function(result){
        console.log(result.value);
        if(result.value == "it-IT"){
          applyLanguage(result.value);
          $translate.use(result.value);
          deferred.resolve();
        }
        else{
          applyLanguage("en-US");
          $translate.use("en-US");
          deferred.resolve();
        }
      })
    }else{
      var language = 'it-IT';
      applyLanguage(language);
      $translate.use(language);
      deferred.resolve();
    }
    return deferred.promise;
  }

  function setLanguage(){
    var promise = setLanguagePromise();
    promise.then(function(){
      //set notification
      setNotification();
    })
  }

	function setFirstAccess(){
		//SALVO LA DATA DEL PRIMO ACCESSO, COSì POSSO PRENDERE TUTTE LE PUSH DALLA DATA DI REGISTRAZIONE IN POI
		if(!localStorage.getItem("firstAccessDateTime")){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;//January is 0, so always add + 1

			var yyyy = today.getFullYear();
			if(dd<10){dd='0'+dd}
			if(mm<10){mm='0'+mm}

			var hour = today.getHours();
			var min = today.getMinutes();
			var seconds = today.getSeconds();
			if(hour<10){hour='0'+hour}
			if(min<10){min='0'+min}
			if(seconds<10){seconds='0'+seconds}

			todayString = {
				firstAccessDateTime : yyyy+'-'+mm+'-'+dd+'T'+hour+':'+min+':'+seconds
			};

			localStorage.setItem("firstAccessDateTime", JSON.stringify(todayString));
		}
	}

  function setNotification(){
    //Notification
    var push = PushNotification.init({
      "android": {
        "senderID": "289107125737"
      },
      "ios": {
        alert: "true",
        badge: true,
        sound: 'false'
      }
    });
    PushNotification.hasPermission(function(data) {
      if (data.isEnabled) {

        push.on('registration', function(data) {
          // prendo la piattaforma del dispositivo Android,Ios ...
          var deviceInformation = ionic.Platform.device();

          var config = {
            headers : {
              'Content-Type': 'application/x-www-form-urlencoded;'
            }
          };

          // inserisco i dati per la request a wordpress
          var data = "token="+data.registrationId + "&os=" + deviceInformation.platform + "&lang=" + $filter('translate')('WP_LANGUAGE');

          // Request per registrare il token
          $http.post("https://idromecspa.com/pnfw/register/", data, config);
        });

        push.on('notification', function(data) {

		    	$state.go('app.notification');

          /*
					var config = {
            headers : {
              'Content-Type': 'application/x-www-form-urlencoded;'
            }
          };

					var language = $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')

          $http.get("https://idromecspa.com/"+ language +"/wp-json/wp/v2/posts/" + data.additionalData.id, config)
          .then(function(data){
            if(data.data.acf.id_macchina){
              $state.go('app.wordpress_pages.balers_detail',{"id": data.data.acf.id_macchina});
            }
            else{
              console.log("NO ID");
            }
          })
          .catch(function(err){
            console.log(err);
          });
					*/
        });

        push.on('error', function(e) {
          //console.log(e.message);
        });

        push.clearAllNotifications(function() {
          console.log('success');
        }, function() {
          console.log('error');
        });
      }
    });
  }

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      // StatusBar.styleDefault();
      ionic.Platform.ready(function(){
        if(ionic.Platform.isIOS()){
          StatusBar.styleLightContent();
        }else{
           StatusBar.styleDefault();
        }
      });
    }

		//setto il primo accesso
		setFirstAccess();

    // set language according to the User’s device settings
    setLanguage();

  });
})

.config(['tmhDynamicLocaleProvider','$translateProvider','defaultLanguage',function(tmhDynamicLocalProvider, $translateProvider, defaultLanguage){
  tmhDynamicLocalProvider.localeLocationPattern('locales/angular-locale_{{locale}}.js');

    $translateProvider.useStaticFilesLoader({
        prefix: 'i18n/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage(defaultLanguage);
}])

.config(function($stateProvider) {
  $stateProvider
    .state('app', {
      url: '/',
      abstract: true,
      template: '<ion-nav-view></ion-nav-view>'
  })
})

/* ----- TOGGLE ----- */
.directive('ionItemAccordion', function($log) {
  return {
    restrict: 'E',
    replace: true,
    transclude: true,
    require: '^ionList',
    scope: {
      title: '@',
      iconClose: '@',
      iconOpen: '@',
      iconAlign: '@'
    },
    template: '<div><ion-item ng-class="classItem()" ng-click="toggleGroup(id)" ng-class="{active: isGroupShown(id)}">' +
      '<i class="icon" ng-class="classGroup(id)"></i>' +
      '&nbsp;' +
      '{{title}}' +
      '</ion-item>' +
      '<ion-item class="item-accordion" ng-show="isGroupShown(id)"><ng-transclude></ng-transclude></ion-item></div>',
    link: function(scope, element, attrs, ionList) {

      // link to parent
      if (!angular.isDefined(ionList.activeAccordion)) ionList.activeAccordion = false;
      if (angular.isDefined(ionList.counterAccordion)) {
        ionList.counterAccordion++;
      } else {
        ionList.counterAccordion = 1;
      }
      scope.id = ionList.counterAccordion;

      // set defaults
      if (!angular.isDefined(scope.id)) $log.error('ID missing for ion-time-accordion');
      if (!angular.isString(scope.title)) $log.warn('Title missing for ion-time-accordion');
      if (!angular.isString(scope.iconClose)) scope.iconClose = 'ion-minus';
      if (!angular.isString(scope.iconOpen)) scope.iconOpen = 'ion-plus';
      if (!angular.isString(scope.iconAlign)) scope.iconAlign = 'left';

      scope.isGroupShown = function() {
        return (ionList.activeAccordion == scope.id);
      };

      scope.toggleGroup = function() {
        $log.debug('toggleGroup');
        if (ionList.activeAccordion == scope.id) {
          ionList.activeAccordion = false;
        } else {
          ionList.activeAccordion = scope.id;
        }
      };

      scope.classGroup = function() {
        return (ionList.activeAccordion == scope.id) ? scope.iconOpen : scope.iconClose;
      };

      scope.classItem = function() {
        return 'item-stable ' + (scope.iconAlign == 'left' ? 'item-icon-left' : 'item-icon-right');
      };
    }
  };
})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.views.swipeBackEnabled(false);
})

.run(function($state) {
  $state.go('app.splash');
});
