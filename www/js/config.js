angular.module("idromec.config", [])

.constant("baseWordpressUrl", "https://idromecspa.com")


.constant("wordPressFormObject", {
	"it": {
		"_wpcf7": "87561",
		"_wpcf7_version": "4.4.2",
		"_wpcf7_locale": "it_IT",
		"_wpcf7_unit_tag": "wpcf7-f87561-p87563-o1",
		"_wpnonce": "f6ebded073",
		"_wpcf7_is_ajax_call": "1"
	},
	"en": {
		"_wpcf7": "87562",
		"_wpcf7_version": "4.4.2",
		"_wpcf7_locale": "it_IT",
		"_wpcf7_unit_tag": "wpcf7-f87562-p87563-o1",
		"_wpnonce": "769d58707d",
		"_wpcf7_is_ajax_call": "1"
	}
});
