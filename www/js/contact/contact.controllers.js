(function() {
  'use strict';

  angular.module('idromec.contact.controllers', [])

  .controller('ContactCtrl', function($scope, $state, $window, $http, $ionicLoading, $filter, $ionicPopup, $cordovaLaunchNavigator, wordPressFormObject, baseWordpressUrl, $cordovaEmailComposer, uiGmapIsReady) { 

    $scope.hideTextMap = true;
    $scope.map = {
      options: {
        scrollwheel: false,
        animation: 'DROP'
      },
      center: {
        latitude: 45.302315,
        longitude: 10.8126661
      },
      marker: {
        key: '1',
        coords: {
          latitude: 45.302315,
          longitude: 10.8126661
        }
      },
      zoom: 16
    };

    uiGmapIsReady.promise(1).then(function(instances) {
      
    })
    .catch(function(error){
      $scope.hideTextMap = false;
    });
    
    
    $scope.callPhone = function(){
      $window.open("tel:+390457930179")
    }

    $scope.launchNavigation = function() { 
      $ionicLoading.show({
        template: '<ion-spinner icon="crescent"></ion-spinner>'
      });

      var destination = [45.302315, 10.8126661];
      ionic.Platform.ready(function(){
        if(ionic.Platform.isAndroid()){
          $cordovaLaunchNavigator.navigate(destination, function(){
            $ionicLoading.hide();
          },function(error){
            console.log(error);
          },{})
        }
        else{
          $cordovaLaunchNavigator.navigate(destination)
          .then(function() {
            $ionicLoading.hide();
          })
          .catch(function(err) { 
             $ionicLoading.hide();
          });
        }
      });
    };

    $scope.writeEmail = function() {
      //INFO: deve essere il plugin 0.8.2 altrimenti è buggato. ionic plugin add https://github.com/katzer/cordova-plugin-email-composer.git#0.8.2
      $cordovaEmailComposer.isAvailable().then(function() {
        var email = {
          to: 'info@idromecspa.com',
        };

        $cordovaEmailComposer.open(email).then(null, function () {
          //TODO: errore?
          console.log('no');
        });
      });
    }
    $scope.title = $state.params.title;
    $scope.contact = {};
    $scope.submitContact = function(form) {
      if(form.$valid && $scope.contact.privacy) {
        var contactFormObject = {
            'full-name': $scope.contact.name + " " + $scope.contact.surname,
            'email': $scope.contact.email,
            'message': $scope.contact.request
        }
        angular.extend(contactFormObject, wordPressFormObject[$filter('translate')('WP_LANGUAGE')]);
        $ionicLoading.show({
          template: '<ion-spinner icon="crescent"></ion-spinner>'
        });
        $http({
          method: 'POST',
          url: baseWordpressUrl,
          headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'},
          transformRequest: function(obj) {
              var str = [];
              for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
          },
          data: contactFormObject
        })
        .then(function(data) {
          $ionicPopup.alert({
            title: $filter('translate')('EMAIL_RESULT'),
            template: $filter('translate')('EMAIL_OK')
          })
          .then(function() {
            $state.go('app.menu');
          });
        })
        .catch(function(err) {
          $ionicPopup.alert({
            title: $filter('translate')('EMAIL_RESULT'),
            template: $filter('translate')('EMAIL_ERROR')
          })
        })
        .finally(function() {
          $ionicLoading.hide();
        })
      }
    }
  })
})();