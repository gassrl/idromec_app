(function() {
  'use strict';

  angular.module('idromec.contact', ['idromec.contact.controllers','idromec.contact.routes'])
})();