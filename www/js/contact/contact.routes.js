(function() {
  'use strict';

  angular.module('idromec.contact.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.contact', {
      url: '/contact/:title',
      templateUrl: 'js/contact/views/contact.html',
      controller: 'ContactCtrl'
    })
  });
})();