(function() {
  'use strict';

  angular.module('idromec.credits.controllers', [])

  .controller("CreditsCtrl",function($scope, $state, $cordovaEmailComposer){
  	    
    $scope.writeEmail = function() {
      //INFO: deve essere il plugin 0.8.2 altrimenti è buggato. ionic plugin add https://github.com/katzer/cordova-plugin-email-composer.git#0.8.2
      $cordovaEmailComposer.isAvailable().then(function() {
        var email = {
          to: 'info@gasweb.it',
        };
        $cordovaEmailComposer.open(email).then(null, function () {
          //TODO: errore?
          console.log('no');
        });
      });
    }
  });

})();