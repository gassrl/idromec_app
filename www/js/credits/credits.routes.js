(function() {
  'use strict';

  angular.module('idromec.credits.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.credits', {
      url: 'credits',
      templateUrl: 'js/credits/views/credits.html',
      controller: 'CreditsCtrl'
    })
  });
})();