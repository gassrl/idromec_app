(function() {
  'use strict';

  angular.module('idromec.menu.controllers', [])

  .controller("MenuCtrl",function($scope, $state, $ionicLoading, $filter, $ionicPopup, notificationPage){
    $scope.hideTextField = true;
		
		$scope.numeroNotifiche = 0;
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
		
    notificationPage.notificationApiAll().query({categories:128}).$promise
    .then(function(data) {
		
			//SETTO IL LOCAL STORAGE CON I VALORI DELLE PUSH PRESENTI NEL DB
			var dateNotification = 0;
			var d1 = 0;
			var dateLocalStorage = 0;
			var d2 = 0;
			var actualNotification = true;
		
			angular.forEach(data, function(result){
				dateNotification = result.date;
				d1 = new Date(dateNotification.replace(' ', 'T'));
			
				dateLocalStorage = $.parseJSON(localStorage.getItem("firstAccessDateTime"))["firstAccessDateTime"];
				d2 = new Date(dateLocalStorage.replace(' ', 'T'));
			
				if(localStorage.getItem(result.id)){
					if($.parseJSON(localStorage.getItem(result.id))["viewed"] != undefined){
					  actualNotification = false;
				  }
				}
				if(d1 >= d2 && actualNotification){
					var arr = $.parseJSON(localStorage.getItem(result.id));
					if(arr){
						if(!arr["deleted"]){
							$scope.numeroNotifiche++;
						}
					}
					else{
						$scope.numeroNotifiche++;
					}
				}
				actualNotification = true;
			});
    })
    .finally(function() {
			$scope.hideTextField = false;
      $ionicLoading.hide();
    });
  });

})();