(function() {
  'use strict';

  angular.module('idromec.menu.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.menu', {
      url: 'menu',
      templateUrl: 'js/menu/views/menu.html',
      controller: 'MenuCtrl'
    })
  });
})();