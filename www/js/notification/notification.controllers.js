(function() {
  'use strict';

  angular.module('idromec.notification.controllers', [])

  .controller("NotificationCtrl",function($scope, $filter, $state, $ionicLoading, $ionicPopup, notificationPage){

	  $scope.delete = function(item) {
			console.log(item);
			if(localStorage.getItem(item.id)){
				var arr = $.parseJSON(localStorage.getItem(item.id));
				arr["deleted"] = "true";
	      localStorage.setItem(item.id, JSON.stringify(arr));
				$scope.notification.splice($scope.notification.indexOf(item), 1);
			}
	  };

    $scope.hideTextMap = true;
    var hasMoreData = true;
		$scope.notification = [];
    $scope.requestPage = 0;

    /* PAGINAZIONE */
    $scope.loadMoreData = function(){
      $scope.requestPage++;
      $ionicLoading.show({
        template: '<ion-spinner icon="crescent"></ion-spinner>'
      });
      if (hasMoreData) {
        notificationPage.notificationApi().query({page: $scope.requestPage, categories:128}).$promise
        .then(function(data) {
          hasMoreData = data.length !== 0;

  				//SETTO IL LOCAL STORAGE CON I VALORI DELLE PUSH PRESENTI NEL DB
  				angular.forEach(data, function(result){
  					var dateNotification = result.date;
  					var d1 = new Date(dateNotification.replace(' ', 'T'));

  					var dateLocalStorage = $.parseJSON(localStorage.getItem("firstAccessDateTime"))["firstAccessDateTime"];
  					var d2 = new Date(dateLocalStorage.replace(' ', 'T'));
  					if(d1 >= d2){
  						if(localStorage.getItem(result.id)){
  							var arr = $.parseJSON(localStorage.getItem(result.id));
  							if(!arr["deleted"]){
  								$scope.notification = $scope.notification.concat($.parseJSON(localStorage.getItem(result.id)));
  							}
  						}
  						else{
  							$scope.notification = $scope.notification.concat(result);
  							localStorage.setItem(result.id, JSON.stringify(result));
  						}
  					}
  				});
        })
        .catch(function(err){
          $scope.requestPage = 0;
          hasMoreData = false;
          // $ionicPopup.alert({
          //   title:  $filter('translate')('NO_CONNECTION.TITLE'),
          //   template: $filter('translate')('NO_CONNECTION.CONTENT')
          // })
          // .then(function() {
          //   $state.go('app.menu');
          // });
        })
        .finally(function() {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $ionicLoading.hide();
        	$scope.hideTextMap = false;
        });
      }
      else {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicLoading.hide();
      }

    };
    $scope.$on('$stateChangeSuccess', function(event, state) {
      if (state.name === 'app.notification') {
        hasMoreData = true;
        $scope.requestPage = 0;
        $scope.loadMoreData();
      }
    });

    $scope.moreDataCanBeLoaded = function(){
      return hasMoreData;
    };

    $scope.setLocalStorage = function(id){
			var arr = $.parseJSON(localStorage.getItem(id));
			arr["viewed"] = "true";
      localStorage.setItem(id, JSON.stringify(arr));
    };

		$scope.getViewedNotification = function(id){
			var arr = $.parseJSON(localStorage.getItem(id));
			if(arr["viewed"]){
				return true;
			}
			else{
				return false;
			}
    };

	});
})();
