(function() {
  'use strict';

  angular.module('idromec.notification', ['idromec.notification.controllers','idromec.notification.routes','idromec.notification.services'])

})();