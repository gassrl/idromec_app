(function() {
  'use strict';

  angular.module('idromec.notification.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.notification', {
      url: 'notification',
      templateUrl: 'js/notification/views/notification.html',
      controller: 'NotificationCtrl'
    })
  });
})();