(function() {
  'use strict';

  angular.module('idromec.notification.services', [])

	.factory('notificationPage', function(baseWordpressUrl, $resource ,$filter) {

	  return {
	    notificationApi: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/posts/:id', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
						headers: {
						  'Cache-Control' : 'no-cache'
						},
		        params: {
		          id: '',
        			per_page: 20,
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    },
	    notificationApiAll: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/posts/:id', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
						headers: {
						  'Cache-Control' : 'no-cache'
						},
		        params: {
		          id: '',
        			per_page: 20,
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    }
	  };
		
	});
})();