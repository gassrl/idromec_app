(function() {
  'use strict';

  angular.module('idromec.splash.controllers', [])

  .controller("SplashCtrl",function($scope, $ionicPopup, $filter, $window, $translate,$cordovaGeolocation, tmhDynamicLocale, $state, $cordovaLaunchNavigator, $cordovaEmailComposer, $ionicLoading){
    
  	$scope.changeLanguage = function() {
      var otherLanguage = $filter('translate')('OTHER_LANGUAGE');
      $translate.use(otherLanguage).then(function() {
        tmhDynamicLocale.set(otherLanguage).then(function() {
          $state.go('app.splash');
        });
      });
    }
    
    $scope.callPhone = function(){
      $window.open("tel:+390457930179")
    }

    $scope.launchNavigation = function() {
      $ionicLoading.show({
        template: '<ion-spinner icon="crescent"></ion-spinner>'
      });
      
      var destination = [45.302315, 10.8126661];
      ionic.Platform.ready(function(){
        if(ionic.Platform.isAndroid()){

          launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function(isAvailable){
            var app;
            if(isAvailable){
                app = launchnavigator.APP.GOOGLE_MAPS;
            }else{
                console.warn("Google Maps not available - falling back to user selection");
                app = launchnavigator.APP.USER_SELECT;
            }
            launchnavigator.navigate(destination, function(){
              $ionicLoading.hide();
            },function(error){
              console.log("errore");
              $ionicLoading.hide();
            },{app: app})
          });
        }
        else{
          $cordovaLaunchNavigator.navigate(destination)
          .then(function() {
            $ionicLoading.hide();
          })
          .catch(function(err) { 
            $ionicLoading.hide();
          });
        }
      });
    };

    $scope.goURL = function(){
    	$state.go('app.menu');
    }
	
	$scope.disabledButton = true;
	
	
	function onOffline() {
        $ionicPopup.alert({
          title:  $filter('translate')('NO_CONNECTION.TITLE'),
          template: $filter('translate')('NO_CONNECTION.CONTENT')
        });
		$scope.disabledButton = true;
	}
	
	function onOnline() {
      $scope.disabledButton = false;
	}
	
	document.addEventListener("offline", onOffline, false);
	document.addEventListener("online", onOnline, false);

    $scope.writeEmail = function() {
    //INFO: deve essere il plugin 0.8.2 altrimenti è buggato. ionic plugin add https://github.com/katzer/cordova-plugin-email-composer.git#0.8.2
    $cordovaEmailComposer.isAvailable().then(function() {
      var email = {
        to: 'info@idromecspa.com'
      };

      $cordovaEmailComposer.open(email).then(null, function () {
        //TODO: errore?
        console.log('no');
      }).catch(function(){
        debugger
      });
    });
   }

  });

})();