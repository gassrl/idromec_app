(function() {
  'use strict';

  angular.module('idromec.splash.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.splash', {
      url: 'splash',
      templateUrl: 'js/splash/views/splash.html',
      controller: 'SplashCtrl'
    })
  });
})();