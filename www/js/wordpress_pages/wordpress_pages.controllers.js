(function() {
  'use strict';

  angular.module('idromec.wordpress_pages.controllers', [])

  .controller('WhoWeAreCtrl', function($scope, $filter, $state, $ionicLoading, WordpressPages, $ionicPopup) {

    $scope.hideButton = true;
    $scope.imageStyle = "";
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    WordpressPages.whoWheAre().query({}).$promise
    .then(function(data) {
      $scope.text = data[0].acf.testo_app;
      $scope.imageUrl = data[0].acf.immagine_app;
      $scope.imageStyle = "background-image: url('" + $scope.imageUrl + "')";
      $scope.hideButton = false;
    })
    .catch(function(err) {
      $ionicPopup.alert({
        title:  $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('IdromecCtrl', function($scope, $filter, $state, $ionicLoading, WordpressPages, $ionicPopup) {
    $scope.imageStyle = "";
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    WordpressPages.idromec().query({}).$promise
    .then(function(data) {
      if ($state.current.name === 'app.wordpress_pages.history') {
        $scope.title = $filter('translate')('HISTORY');
        $scope.text = data[0].acf.testo_storia_app;
        $scope.imageUrl = data[0].acf.immagine_storia_app;
        $scope.imageStyle = "background-image: url('" + $scope.imageUrl + "')";
      }
      else if ($state.current.name === 'app.wordpress_pages.mission') {
        $scope.title = $filter('translate')('MISSION');
        $scope.text = data[0].acf.testo_mission_app;
        $scope.imageUrl = data[0].acf.immagine_mission_app;
        $scope.imageStyle = "background-image: url('" + $scope.imageUrl + "')";
      }
      else if ($state.current.name === 'app.wordpress_pages.vision') {
        $scope.title = $filter('translate')('VISION');
        $scope.text = data[0].acf.testo_vision_app;
        $scope.imageUrl = data[0].acf.immagine_vision_app;
        $scope.imageStyle = "background-image: url('" + $scope.imageUrl + "')";
      }
    })
    .catch(function(err) {
      $ionicPopup.alert({
        title:  $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
      console.log(err);
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('NewsCtrl', function($scope, $filter, $state, $ionicLoading, WordpressPages, $ionicPopup) {

    var hasMoreData = true;
    $scope.news = [];
    $scope.requestPage = 0;
    /* PAGINAZIONE */
    $scope.loadMoreData = function(){
      $scope.requestPage++;
      $ionicLoading.show({
        template: '<ion-spinner icon="crescent"></ion-spinner>'
      });
      if (hasMoreData) {
        WordpressPages.newsWordpressApi().query({page: $scope.requestPage, categories:105}).$promise
        .then(function(data) {
          hasMoreData = data.length !== 0;
          $scope.news =  $scope.news.concat(data);
        })
        .catch(function(err) {
          $scope.requestPage = 0;
          hasMoreData = false;
          // $ionicPopup.alert({
          //   title:  $filter('translate')('NO_CONNECTION.TITLE'),
          //   template: $filter('translate')('NO_CONNECTION.CONTENT')
          // })
          // .then(function() {
          //   $state.go('app.menu');
          // });
        })
        .finally(function() {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $ionicLoading.hide();
        });
      }
      else {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicLoading.hide();
      }
    };

    $scope.$on('$stateChangeSuccess', function(event, state) {
      if (state.name === 'app.wordpress_pages.news') {
        $scope.requestPage = 0;
        hasMoreData = true;
        $scope.loadMoreData();
      }
    });

    $scope.moreDataCanBeLoaded = function(){
      return hasMoreData;
    };
  })

  .controller('NewsDetailCtrl', function($scope, $filter, $state, $ionicLoading, WordpressPages, $ionicPopup) {
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    WordpressPages.newsWordpressApi().get({ id: $state.params.id }).$promise
    .then(function(data) {
      $scope.news = data;
    })
    .catch(function(err) {
      $ionicPopup.alert({
        title:  $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('BalersCtrl', function($scope, $filter, $q, $state, $ionicLoading, WordpressPages, $ionicPopup) {

    $scope.title = $state.params.title;

    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    var balersListDeferred = $q.defer();
    WordpressPages.balersWordpressApi().query({ 'slug': $state.params.slug }).$promise
    .then(function(data) {
      WordpressPages.balersWordpressApi().subPages({ 'parent': data[0].id }).$promise
      .then(function(balersSubPages) {
        $scope.balers = balersSubPages;
        balersListDeferred.resolve();
      })
      .catch(function(err) {
        balersListDeferred.reject();
        $ionicPopup.alert({
          title: $filter('translate')('NO_CONNECTION.TITLE'),
          template: $filter('translate')('NO_CONNECTION.CONTENT')
        })
        .then(function() {
          $state.go('app.menu');
        });
      });
    })
    .catch(function(err) {
      balersListDeferred.reject();
      $ionicPopup.alert({
        title: $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    balersListDeferred.promise
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('BalersTypeCtrl', function($scope, $filter, $state, $ionicLoading, WordpressPages, $ionicPopup) {
    $scope.title = $state.params.title;
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    WordpressPages.balersWordpressApi().subPages({ 'parent': $state.params.id }).$promise
    .then(function(balersSubPages) {
      $scope.balersType = balersSubPages;
    })
    .catch(function(err) {
      $ionicPopup.alert({
        title: $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('BalersNoSubcategoryCtrl', function($scope, $filter, $q, $state, $ionicLoading, WordpressPages, $ionicPopup) {
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    var productListDeferred = $q.defer();
    WordpressPages.balersWordpressApi().query({'slug': $state.params.slug}).$promise
    .then(function(data) {
      WordpressPages.balersWordpressApi().subPages({ 'parent': data[0].id }).$promise
      .then(function(productSubPages) {
        $scope.balers = productSubPages;
        productListDeferred.resolve();
      })
      .catch(function(err) {
        productListDeferred.reject();
      });
    })
    .catch(function(err) {
      productListDeferred.reject();
      $ionicPopup.alert({
        title: $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    productListDeferred.promise
    .finally(function() {
      $ionicLoading.hide();
    });
  })

  .controller('BalersDetailCtrl', function($scope, $filter, $q, $state, $ionicLoading, WordpressPages, $ionicSlideBoxDelegate, $ionicPopup) {
    $scope.hideElement = true;
    $ionicLoading.show({
      template: '<ion-spinner icon="crescent"></ion-spinner>'
    });
    WordpressPages.balersWordpressApi().get({ 'id': $state.params.id }).$promise
    .then(function(data) {
      $scope.balersDetail = data;
      $scope.hideElement = false;
    })
    .catch(function(err) {
      $ionicPopup.alert({
        title: $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });

    $scope.onReadySwiper = function(swiper){
      swiper.initObservers();
    };
  })

  .controller('UsedCtrl', function($scope, $filter, $state, $q, $ionicLoading, WordpressPages, $ionicPopup){
    $scope.requestPage = 0;
    $scope.count = 1;
    $scope.used = [];
    var hasMoreData = false;

    var usedIdDeferred = $q.defer();
    WordpressPages.usedWordpressApi().query().$promise
    .then(function(data) {
      usedIdDeferred.resolve(data[0].id);
      $scope.loadMoreData();
    })
    .catch(function(err) {
      usedIdDeferred.reject();
      $ionicPopup.alert({
        title: $filter('translate')('NO_CONNECTION.TITLE'),
        template: $filter('translate')('NO_CONNECTION.CONTENT')
      })
      .then(function() {
        $state.go('app.menu');
      });
    });



    /* PAGINAZIONE */
    $scope.loadMoreData = function(){
      $ionicLoading.show({
        template: '<ion-spinner icon="crescent"></ion-spinner>'
      });
      $scope.requestPage++;
      usedIdDeferred.promise
      .then(function(data){
        WordpressPages.usedWordpressApi().subPagesPagination({ 'page': $scope.requestPage ,'parent': data }).$promise
        .then(function(usedSubPages){
          hasMoreData = usedSubPages.length !== 0;
          $scope.used =  $scope.used.concat(usedSubPages);
          $scope.$broadcast('scroll.infiniteScrollComplete');
        })
        .catch(function(err){
          hasMoreData = false;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        })
        .finally(function() {
          $ionicLoading.hide();
        });
      })
      .catch(function(err){
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: $filter('translate')('NO_CONNECTION.TITLE'),
          template: $filter('translate')('NO_CONNECTION.CONTENT')
        })
        .then(function(){
          $state.go('app.menu');
        });
      })
    };

    $scope.moreDataCanBeLoaded = function(){
      return hasMoreData;
    };

  });
})();
