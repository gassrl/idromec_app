(function() {
  'use strict';

  angular.module('idromec.wordpress_pages', ['idromec.wordpress_pages.controllers','idromec.wordpress_pages.routes','idromec.wordpress_pages.services'])

})();