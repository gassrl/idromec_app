(function() {
  'use strict';

  angular.module('idromec.wordpress_pages.routes', [])

  .config(function($stateProvider) {
    $stateProvider

    .state('app.wordpress_pages', {
      url: '/wordpress_pages',
      abstract: true,
      template: '<ion-nav-view></ion-nav-view>'
    })
    .state('app.wordpress_pages.who_we_are', {
      url: '/who_we_are',
      templateUrl: 'js/wordpress_pages/views/who_we_are.html',
      controller: 'WhoWeAreCtrl'
    })
    .state('app.wordpress_pages.history', {
      url: '/history',
      templateUrl: 'js/wordpress_pages/views/idromec.html',
      controller: 'IdromecCtrl'
    })
    .state('app.wordpress_pages.mission', {
      url: '/mission',
      templateUrl: 'js/wordpress_pages/views/idromec.html',
      controller: 'IdromecCtrl'
    })
    .state('app.wordpress_pages.vision', {
      url: '/vision',
      templateUrl: 'js/wordpress_pages/views/idromec.html',
      controller: 'IdromecCtrl'
    })
    .state('app.wordpress_pages.news', {
      url: '/news',
      templateUrl: 'js/wordpress_pages/views/news.html',
      controller: 'NewsCtrl'
    })
    .state('app.wordpress_pages.news_detail', {
      url: '/news/:id',
      templateUrl: 'js/wordpress_pages/views/news_detail.html',
      controller: 'NewsDetailCtrl'
    })
    .state('app.wordpress_pages.balers', {
      url: '/balers/:slug/:title',
      templateUrl: 'js/wordpress_pages/views/balers.html',
      controller: 'BalersCtrl'
    })
    .state('app.wordpress_pages.balers_type', {
      url: '/balers_type/:id/:title',
      templateUrl: 'js/wordpress_pages/views/balers_type.html',
      controller: 'BalersTypeCtrl'
    })
    .state('app.wordpress_pages.balers_no_subcategory', {
      url: '/balers_no_subcategory/:slug',
      templateUrl: 'js/wordpress_pages/views/balers_no_subcategory.html',
      controller: 'BalersNoSubcategoryCtrl'
    })
    .state('app.wordpress_pages.balers_detail', {
      url: '/balers_detail/:id',
      templateUrl: 'js/wordpress_pages/views/balers_detail.html',
      controller: 'BalersDetailCtrl'
    })
    .state('app.wordpress_pages.used', {
      url: '/used',
      templateUrl: 'js/wordpress_pages/views/used.html',
      controller: 'UsedCtrl'
    })
  });
})();