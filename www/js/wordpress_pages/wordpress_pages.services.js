(function() {
  'use strict';

  angular.module('idromec.wordpress_pages.services', [])

	.factory('WordpressPages', function(baseWordpressUrl, $resource ,$filter) {

	  return {
	    whoWheAre: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/pages', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
		          slug: $filter('translate')('WHO_WE_ARE.URL'),
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    },
	    idromec: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/pages', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
		          slug: 'idromec',
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    },
	    newsWordpressApi: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/posts/:id', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
		          id: '',
        			per_page: 5,
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    },
	    balersWordpressApi: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/pages/:id', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
		          slug: '',
          		id: '',
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      },
		      'subPages': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
          		id: '',
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    },
	    usedWordpressApi: function() {
	      return $resource(baseWordpressUrl + '/:language/wp-json/wp/v2/pages/:id', {}, {
		      'query': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
		          slug: $filter('translate')('USED.URL'),
        			id: '',
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      },
		      'subPages': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
          		id: '',
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      },
		      'subPagesPagination': {
		        method: 'GET',
		        cache: false,
		        isArray: true,
		        params: {
          		id: '',
        			per_page: 5,
		          language: $filter('translate')('WP_LANGUAGE') == 'it' ? '' : $filter('translate')('WP_LANGUAGE')
		        }
		      }
		    });
	    }
	  };
	});

})();